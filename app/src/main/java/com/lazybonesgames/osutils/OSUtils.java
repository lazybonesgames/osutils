//+package ${YYAndroidPackageName};
/*-*/package com.lazybonesgames.osutils;

//+import ${YYAndroidPackageName}.R;
//+import com.yoyogames.runner.RunnerJNILib;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.SparseArray;

import androidx.annotation.NonNull;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class OSUtils {

    private final static int EVENT_OTHER_SOCIAL = 70;
    private final static int ASYNC_RESPONSE_BITMAP_READY = 85472;

    public static void share(String subject, String message) {
        Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, subject);
        shareIntent.putExtra(Intent.EXTRA_TEXT, message);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        else
            shareIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        RunnerActivity.CurrentActivity.startActivity(Intent.createChooser(shareIntent, "Share"));
    }

    public static void rateApp() {
        openStorePage();
    }

    public static void openStorePage() {
        Uri uri = Uri.parse("market://details?id=" + BuildConfig.APPLICATION_ID);
        Intent rateIntent = new Intent(Intent.ACTION_VIEW, uri);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            rateIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_NEW_DOCUMENT |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        else
            rateIntent.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                    Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                    Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            RunnerActivity.CurrentActivity.startActivity(rateIntent);
        }
        catch (ActivityNotFoundException e) {
            rateIntent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID));
            RunnerActivity.CurrentActivity.startActivity(rateIntent);
        }
    }

    public static String getOSVersion() {
        return Build.VERSION.RELEASE;
    }

    public static String getDeviceName() {
        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;
        if (model.startsWith(manufacturer))
            return capitalize(model);
        else
            return capitalize(manufacturer) + " " + model;
    }
    private static String capitalize(String s) {
        if (s == null || s.length() == 0) {
            return "";
        }
        char first = s.charAt(0);
        if (Character.isUpperCase(first)) {
            return s;
        } else {
            return Character.toUpperCase(first) + s.substring(1);
        }
    }

    //region async sprite loading
    private final static SparseArray<Bitmap> decodedBitmaps = new SparseArray<>(0);
    private static int bitmapsCounter = 0;
    private final static List<Integer> bitmapReadyEventsSync = Collections.synchronizedList(new ArrayList<Integer>(0));
    private static final Handler.Callback recycleBitmapCallback = new Handler.Callback() {
        @Override
        public boolean handleMessage(@NonNull Message msg) {
            recycleBitmap(msg.what); //recycle bitmap if it wasn't consumed by GML yet
            return true;
        }
    };
    public static double loadExternalSpriteAsync(final String spritePath) {
        final int bitmapIndex = bitmapsCounter;
        bitmapsCounter++;
        new Thread(() -> {
            final Bitmap bitmap = BitmapFactory.decodeFile(RunnerActivity.CurrentActivity.getFilesDir().getAbsolutePath() + "/" + spritePath);

            int bitmapReadyEvent = createDsMap();
            addAsyncMapValueDouble(bitmapReadyEvent, "id", ASYNC_RESPONSE_BITMAP_READY);
            addAsyncMapValueDouble(bitmapReadyEvent, "index", bitmapIndex);

            if (bitmap == null)
                addAsyncMapValueDouble(bitmapReadyEvent, "size", 0);
            else {
                synchronized (decodedBitmaps) {
                    decodedBitmaps.put(bitmapIndex, bitmap);
                }
                addAsyncMapValueDouble(bitmapReadyEvent, "size", bitmap.getByteCount());
                addAsyncMapValueDouble(bitmapReadyEvent, "width", bitmap.getWidth());
                addAsyncMapValueDouble(bitmapReadyEvent, "height", bitmap.getHeight());

                Handler handler = new Handler(Looper.getMainLooper(), recycleBitmapCallback);
                handler.sendEmptyMessageDelayed(bitmapIndex, 10000);
            }

            if (bitmapReadyEventsSync.isEmpty()) {
                bitmapReadyEventsSync.add(bitmapReadyEvent);
                synchronized (bitmapReadyEventsSync) {
                    while (!bitmapReadyEventsSync.isEmpty()) {
                        sendAsyncEvent(bitmapReadyEventsSync.get(0));
                        bitmapReadyEventsSync.remove(0);
                    }
                }
            }
            else
                bitmapReadyEventsSync.add(bitmapReadyEvent);
        }).start();
        return bitmapIndex;
    }

    private static synchronized int createDsMap() {
        return RunnerJNILib.jCreateDsMap(null, null, null);
    }

    public static void recycleBitmap(double bitmapIndex) {
        int bitmapInd = (int)Math.round(bitmapIndex);
        Bitmap bitmap = decodedBitmaps.get(bitmapInd);
        if (bitmap != null) {
            decodedBitmaps.delete(bitmapInd);
            bitmap.recycle();
        }
    }

    public static double fillBitmapBuffer(ByteBuffer buffer, double bitmapIndex) {
        int bitmapInd = (int)Math.round(bitmapIndex);
        Bitmap bitmap = decodedBitmaps.get(bitmapInd);
        if (bitmap == null)
            return 0;
        decodedBitmaps.delete(bitmapInd);

        bitmap.copyPixelsToBuffer(buffer);
        bitmap.recycle();
        return 1;
    }
    //endregion

    public static String getPreferredABI() {
        return Build.SUPPORTED_ABIS[0];
    }



    //TODO !!!!! debug keys in crashlytics. Remove when issue with async_load in GM will be resolved
    //private final static JSONArray asyncKeysRoot = new JSONArray();

    static void addAsyncMapValueDouble(int map, String key, double value) {
        RunnerJNILib.DsMapAddDouble(map, key, value);
        //synchronized (asyncKeysRoot) {
        //    if (asyncKeysRoot.length() == 0)
        //        asyncKeysRoot.put(new JSONObject());
        //    try {
        //        JSONObject asyncKeys = asyncKeysRoot.getJSONObject(0);
        //        asyncKeys.put(key, value);
        //    } catch (JSONException e) {
        //        e.printStackTrace();
        //    }
        //}
    }

    static void sendAsyncEvent(int mapIndex) {
        //if (RunnerActivity.CurrentActivity != null) {
        //    synchronized (asyncKeysRoot) {
        //        if (asyncKeysRoot.length() != 0) {
        //            try { //Crachlytics.gml parse "async_custom_keys" file
        //                JSONObject asyncKeys = asyncKeysRoot.getJSONObject(0);
        //                FileWriter writer = new FileWriter(RunnerActivity.CurrentActivity.getFilesDir().getAbsolutePath() + "/async_custom_keys");
        //                writer.write(asyncKeys.toString());
        //                asyncKeysRoot.remove(0);
        //                writer.close();
        //            } catch (IOException | JSONException e) {
        //                e.printStackTrace();
        //            }
        //        }
        //    }
        //}

        RunnerJNILib.CreateAsynEventWithDSMap(mapIndex, EVENT_OTHER_SOCIAL);
    }

}
